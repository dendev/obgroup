<?php

use Doctum\Doctum;
use Symfony\Component\Finder\Finder;
use Doctum\RemoteRepository\GitHubRemoteRepository;

$iterator = Finder::create()
    ->files()
    ->name('*.php')
    ->exclude('resources')
    ->exclude('tests')
    ->in('./src');


return new Doctum($iterator, [
    'title'                => 'Obgroup',
    'language'             => 'fr', // Could be 'fr'
    'build_dir'            => __DIR__ . '/docs/build',
    'cache_dir'            => __DIR__ . '/docs/cache',
    'remote_repository'    => new GitHubRemoteRepository('dendev/obgroup', 'https://gitlab.com/dendev/obgroup'),
    'default_opened_level' => 2,
]);
