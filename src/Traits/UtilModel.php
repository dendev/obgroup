<?php

namespace Dendev\Obgroup\Traits;

use Illuminate\Database\Eloquent\Collection;

trait UtilModel
{
    public static function soak(array $values, array $to_loads = []): Collection
    {
        $collection = collect();

        $model_classname = self::class;

        // array values to models collection
        $collection = $model_classname::hydrate($values);

        // load relations
        if( count( $to_loads ) > 0 )
        {
            // check relation availability
            $model = new $model_classname();
            foreach( $to_loads as $key => $to_load )
            {
                if( ! $model->isRelation($to_load) )
                    unset($to_loads[$key]);
            }

            // load relations
            $collection->load($to_loads);
        }


        return $collection;
    }

}
