<?php

namespace Dendev\Obgroup\Traits;

use Exception;
use Illuminate\Support\Facades\DB;

trait UtilSeeder
{
    protected function _reset_table(string $table_name, $where = false): void
    {
        $is_pgsql = env('DB_IS_PGSQL', false);

        if( $is_pgsql )
        {
            /*
            DB::statement("alter table $table_name disable trigger all"); //TODO SQLSTATE[42501]: Insufficient privilege: 7 ERROR:  permission denied: "RI_ConstraintTrigger_c_20810" is a system trigger (Connection: pgsql, SQL: alter table memberships disable trigger all)
            DB::statement("alter table $table_name enable trigger all");
            */
            if( $where )
                DB::table($table_name)->where($where[0], $where[2])->delete();
            else
                DB::table($table_name)->truncate();
        }
        else
        {
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table($table_name)->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }

    }

    /**
     * @throws Exception
     */
    protected function _fake_seeder(int $min, int $max, string $model_classname): void
    {
        if( env('FAKE_SEEDER_IS_ENABLED', false) )
        {
            $nb = random_int($min, $max);
            $model_classname::factory()->count($nb)->create();
        }
    }
}
