<?php

namespace Dendev\Obgroup\Facades;

use Illuminate\Support\Facades\Facade;

class Obgroup extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'obgroup';
    }
}
