<?php

namespace Dendev\Obgroup\Models;

use Dendev\Obgroup\Traits\HasPackageFactory;
use Dendev\Obgroup\Traits\UtilModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Group extends Model
{
    use HasPackageFactory;
    use UtilModel;

    protected $fillable = [
        'label',
        'category_id',
        'description',
        'order'
    ];
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    *

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS https://laravel.com/docs/9.x/eloquent-relationships
    |--------------------------------------------------------------------------
    */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES https://laravel.com/docs/9.x/eloquent#query-scopes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS https://laravel.com/docs/9.x/eloquent-mutators#defining-an-accessor
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS https://laravel.com/docs/9.x/eloquent-mutators#defining-a-mutator
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | EVENTS https://laravel.com/docs/9.x/events
    |--------------------------------------------------------------------------
    */
     protected static function booted(): void
    {
        static::saving(function (Group $group) {
            if( is_null( $group->identity) || $group->identity === '')
                $group->identity = Str::of($group->label)->snake();

            if( is_null( $group->order) || $group->order === '')
                $group->order = ucfirst((substr(Str::of($group->identity)->snake(), 0, 1)));
        });
    }
}
