<?php

namespace Dendev\Obgroup\Models;

use Dendev\Obgroup\Traits\HasPackageFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Subgroup extends Model
{
    use HasPackageFactory;

    protected $fillable = [
        'label',
        'identity',
        'description',
        'order'
    ];

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    *

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS https://laravel.com/docs/9.x/eloquent-relationships
    |--------------------------------------------------------------------------

    /*
    |--------------------------------------------------------------------------
    | SCOPES https://laravel.com/docs/9.x/eloquent#query-scopes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS https://laravel.com/docs/9.x/eloquent-mutators#defining-an-accessor
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS https://laravel.com/docs/9.x/eloquent-mutators#defining-a-mutator
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | EVENTS https://laravel.com/docs/9.x/events
    |--------------------------------------------------------------------------
    */
     protected static function booted(): void
    {
        static::saving(function (Subgroup $model) {
            if( is_null( $model->identity) || $model->identity === '')
                $model->identity = Str::of($model->label)->snake();

          if( is_null( $model->order) || $model->order === '')
                $model->order = ucfirst((substr(Str::of($model->identity)->snake(), 0, 1)));
        });
    }
}
