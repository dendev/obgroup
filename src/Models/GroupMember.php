<?php

namespace Dendev\Obgroup\Models;

use App\Models\User;
use Dendev\Obgroup\Traits\HasPackageFactory;
use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model
{
    use HasPackageFactory;

    protected $table = 'group_members';

    protected $fillable = [
        'member_id',
        'member_table',
        'group_id',
        'subgroup_id',
        'is_user',
        'user_can_view_members',
        'user_can_remove_member',
        'user_can_add_member',
    ];

    protected $casts = [
        'is_user' => 'boolean'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    *

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function save(array $options = [])
    {
        $this->table = 'group_members';
        return parent::save($options);
    }

    public function update(array $attributes = [], array $options = [])
    {
        $this->table = 'group_members';
        return parent::update($attributes, $options);
    }

    public function delete()
    {
        $this->table = 'group_members';
        return parent::delete();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS https://laravel.com/docs/9.x/eloquent-relationships
    |--------------------------------------------------------------------------
    */
    public function member()
    {
        $refs = [
            'users' => User::class,
        ];

        if( array_key_exists($this->member_table, $refs))
            return $this->belongsTo($refs[$this->member_table], 'member_id');

        return null;
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES https://laravel.com/docs/9.x/eloquent#query-scopes
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS https://laravel.com/docs/9.x/eloquent-mutators#defining-an-accessor
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS https://laravel.com/docs/9.x/eloquent-mutators#defining-a-mutator
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | EVENTS https://laravel.com/docs/9.x/events
    |--------------------------------------------------------------------------
    */
}
