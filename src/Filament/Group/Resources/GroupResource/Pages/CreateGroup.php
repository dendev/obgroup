<?php

namespace Dendev\Obgroup\Filament\Group\Resources\GroupResource\Pages;

use Dendev\Obgroup\Filament\Group\Resources\GroupResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateGroup extends CreateRecord
{
    protected static string $resource = GroupResource::class;
}
