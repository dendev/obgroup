<?php

namespace Dendev\Obgroup\Filament\Group\Resources\GroupResource\Pages;

use Dendev\Obgroup\Filament\Group\Resources\GroupResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListGroups extends ListRecords
{
    protected static string $resource = GroupResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
