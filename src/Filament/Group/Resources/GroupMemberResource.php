<?php

namespace Dendev\Obgroup\Filament\Group\Resources;

use Dendev\Obgroup\Filament\Group\Resources\GroupMemberResource\Pages;
use Dendev\Obgroup\Filament\Group\Resources\GroupMemberResource\RelationManagers;
use Dendev\Obgroup\Models\GroupMember;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class GroupMemberResource extends Resource
{
    protected static ?string $model = GroupMember::class;

    protected static ?string $navigationIcon = 'heroicon-c-squares-plus';
    protected static ?int $navigationSort = 1;


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('group_id')
                    ->required()
                    ->numeric(),
                Forms\Components\TextInput::make('member_id')
                    ->required()
                    ->numeric(),
                Forms\Components\TextInput::make('member_table')
                    ->required()
                    ->maxLength(255),
                Forms\Components\Toggle::make('is_user')
                    ->required(),
                Forms\Components\Toggle::make('user_can_view_members')
                    ->required(),
                Forms\Components\Toggle::make('user_can_remove_member')
                    ->required(),
                Forms\Components\Toggle::make('user_can_add_member')
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('group_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('member_id')
                    ->numeric()
                    ->sortable(),
                Tables\Columns\TextColumn::make('member_table')
                    ->searchable(),
                Tables\Columns\IconColumn::make('is_user')
                    ->boolean(),
                Tables\Columns\IconColumn::make('user_can_view_members')
                    ->boolean(),
                Tables\Columns\IconColumn::make('user_can_remove_member')
                    ->boolean(),
                Tables\Columns\IconColumn::make('user_can_add_member')
                    ->boolean(),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListGroupMembers::route('/'),
            'create' => Pages\CreateGroupMember::route('/create'),
            'edit' => Pages\EditGroupMember::route('/{record}/edit'),
        ];
    }
}
