<?php

namespace Dendev\Obgroup\Filament\Group\Resources;

use Dendev\Obgroup\Filament\Group\Resources\SubgroupResource\Pages;
use Dendev\Obgroup\Filament\Group\Resources\SubgroupResource\RelationManagers;
use Dendev\Obgroup\Models\Subgroup;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class SubgroupResource extends Resource
{
    protected static ?string $model = Subgroup::class;
    protected static ?string $navigationIcon = 'heroicon-c-square-2-stack';
    protected static ?int $navigationSort = 3;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('label')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('identity')
                    ->required()
                    ->maxLength(255),
                Forms\Components\Textarea::make('description')
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('target_table')
                    ->maxLength(255),
                Forms\Components\TextInput::make('order')
                    ->maxLength(255),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('label')
                    ->searchable(),
                Tables\Columns\TextColumn::make('identity')
                    ->searchable(),
                Tables\Columns\TextColumn::make('target_table')
                    ->searchable(),
                Tables\Columns\TextColumn::make('order')
                    ->searchable(),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSubgroups::route('/'),
            'create' => Pages\CreateSubgroup::route('/create'),
            'edit' => Pages\EditSubgroup::route('/{record}/edit'),
        ];
    }
}
