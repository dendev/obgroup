<?php

namespace Dendev\Obgroup\Filament\Group\Resources\SubgroupResource\Pages;

use Dendev\Obgroup\Filament\Group\Resources\SubgroupResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListSubgroups extends ListRecords
{
    protected static string $resource = SubgroupResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
