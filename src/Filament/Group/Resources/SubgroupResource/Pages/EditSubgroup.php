<?php

namespace Dendev\Obgroup\Filament\Group\Resources\SubgroupResource\Pages;

use Dendev\Obgroup\Filament\Group\Resources\SubgroupResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditSubgroup extends EditRecord
{
    protected static string $resource = SubgroupResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
