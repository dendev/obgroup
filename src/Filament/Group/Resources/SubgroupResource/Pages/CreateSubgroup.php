<?php

namespace Dendev\Obgroup\Filament\Group\Resources\SubgroupResource\Pages;

use Dendev\Obgroup\Filament\Group\Resources\SubgroupResource;
use Filament\Resources\Pages\CreateRecord;

class CreateSubgroup extends CreateRecord
{
    protected static string $resource = SubgroupResource::class;
}
