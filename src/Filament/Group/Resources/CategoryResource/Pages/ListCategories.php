<?php

namespace Dendev\Obgroup\Filament\Group\Resources\CategoryResource\Pages;

use Dendev\Obgroup\Filament\Group\Resources\CategoryResource;
use Filament\Actions;
use Filament\Resources\Pages\ListRecords;

class ListCategories extends ListRecords
{
    protected static string $resource = CategoryResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
