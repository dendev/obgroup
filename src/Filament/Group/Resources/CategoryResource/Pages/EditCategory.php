<?php

namespace Dendev\Obgroup\Filament\Group\Resources\CategoryResource\Pages;

use Dendev\Obgroup\Filament\Group\Resources\CategoryResource;
use Filament\Actions;
use Filament\Resources\Pages\EditRecord;

class EditCategory extends EditRecord
{
    protected static string $resource = CategoryResource::class;

    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
