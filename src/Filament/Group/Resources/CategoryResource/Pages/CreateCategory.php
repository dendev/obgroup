<?php

namespace Dendev\Obgroup\Filament\Group\Resources\CategoryResource\Pages;

use Dendev\Obgroup\Filament\Group\Resources\CategoryResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateCategory extends CreateRecord
{
    protected static string $resource = CategoryResource::class;
}
