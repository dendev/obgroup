<?php

namespace Dendev\Obgroup\Filament\Group\Resources\GroupMemberResource\Pages;

use Dendev\Obgroup\Filament\Group\Resources\GroupMemberResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateGroupMember extends CreateRecord
{
    protected static string $resource = GroupMemberResource::class;
}
