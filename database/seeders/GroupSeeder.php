<?php

namespace Dendev\Obgroup\Database\Seeders;

use Dendev\Obgroup\Models\Category;
use Dendev\Obgroup\Models\Group;
use Dendev\Obgroup\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->_reset_table('groups');

        // categories
        $categories = [
            ['identity' => 'economic', 'label' => 'Economique', 'src_id' => 1],
            ['identity' => 'paramedical', 'label' => 'Paramédical', 'src_id' => 2],
            ['identity' => 'pedagogic', 'label' => 'Pédagogique', 'src_id' => 3],
            ['identity' => 'social', 'label' => 'Social', 'src_id' => 4],
            ['identity' => 'technic', 'label' => 'Technique', 'src_id' => 5]
        ];
        foreach( $categories as $key => $category)
            $categories[$key]['category_identity'] = 'category';

        // implantations
        $implantations = [
            ['identity' => 'implantation-hnl', 'label' => 'Haute Ecole de Namur-Liège-Luxembourg', 'src_id' => '0' ],
            ['identity' => 'implantation-iesn', 'label' => 'Départements économique, technique et pédagogique IESN', 'src_id' => 1 ],
            ['identity' => 'implantation-pa', 'label' => 'Département paramédical Sainte Elisabeth', 'src_id' => 2 ],
            ['identity' => 'implantation-ma', 'label' => 'Départements pédagogique et social de Malonne', 'src_id' => 3 ],
            ['identity' => 'implantation-ch', 'label' => 'Département pédagogique de Champion', 'src_id' => 4 ],
            ['identity' => 'implantation-so', 'label' => 'Département social de Namur', 'src_id' => 5 ],
            ['identity' => 'implantation-cn', 'label' => 'Co-organisation Cardijn-Namur', 'src_id' => 6 ],
            ['identity' => 'implantation-pb', 'label' => 'Département pédagogique de Bastogne', 'src_id' => 7 ],
            ['identity' => 'implantation-sa', 'label' => "Départements économique, technique et social d\'Arlon", 'src_id' => 8 ],
            ['identity' => 'implantation-ea', 'label' => "Département électromécanique d\'Arlon", 'src_id' => 9 ],
            ['identity' => 'implantation-es', 'label' => 'Département électromécanique de Seraing', 'src_id' => 10 ],
            ['identity' => 'implantation-iv', 'label' => 'Département ingénieur industriel de Pierrard-Virton', 'src_id' => 11 ],
            ['identity' => 'implantation-sc', 'label' => 'Siège Central - Haute Ecole de Namur-Liège-Luxembourg', 'src_id' => 12 ],
            ['identity' => 'implantation-mi', 'label' => 'Département technique de Marche-en-Famenne', 'src_id' => 13 ],
        ];
        foreach( $implantations as $key => $implantation)
            $implantations[$key]['category_identity'] = 'implantation';

        //  departements
        $departments = [
            ['identity' => 'department_social_arlon', 'label' => "Département social d'Arlon", 'src_id' => 1 ],
            ['identity' => 'department_economic_iesn', 'label' => 'Département économique IESN', 'src_id' => 2 ],
            ['identity' => 'department_paramedical_pa', 'label' => 'Département paramédical Sainte-Elisabeth', 'src_id' => 3 ],
            ['identity' => 'department_pedagogic_pb', 'label' => "Département pédagogique de Bastogne", 'src_id' => 4 ],
            ['identity' => 'department_pedagogic_ch', 'label' => 'Département pédagogique de Champion', 'src_id' => 5 ],
            ['identity' => 'department_pedagogic_iesn', 'label' => 'Département pédagogique IESN', 'src_id' => 6 ],
            ['identity' => 'department_pedagogic_ma', 'label' => 'Département pédagogique de Malonne', 'src_id' => 7 ],
            ['identity' => 'department_social_cn', 'label' => "Département sociaux de Cardijn et de Namur", 'src_id' => 8 ],
            ['identity' => 'department_social_ma', 'label' => 'Département social de Malonne', 'src_id' => 9 ],
            ['identity' => 'department_social_so', 'label' => 'Département social de Namur', 'src_id' => 10 ],
            ['identity' => 'department_technic_iesn', 'label' => 'Département technique IESN', 'src_id' => 11 ],
            ['identity' => 'department_technic_ea', 'label' => "Département technique d'Arlon", 'src_id' => 12 ],
            ['identity' => 'department_electromechanics_es', 'label' => 'Département électromécanique de Seraing', 'src_id' => 13 ],
            ['identity' => 'department_engineer_iv', 'label' => 'Département ingénieur industriel de Pierrard-Virton', 'src_id' => 14 ],
            ['identity' => 'department_technic_mi', 'label' => 'Département technique de Marche-en-Famenne', 'src_id' => 15 ],
            ['identity' => 'department_social_ea', 'label' => "Département social d'Arlon", 'src_id' => 16 ],
            ['identity' => 'department_sc', 'label' => "Siège central", 'src_id' => 17 ],
        ];
        foreach( $departments as $key => $department)
            $departments[$key]['category_identity'] = 'department';

        $sections = [
            ['identity' => 'section_cn_ias', 'label' => "Co-organisation Cardijn-Namur / Ingénierie et action sociales (IAS)", 'src_id' =>  50 ],
            ['identity' => 'section_iv_msiia', 'label' => "Département ingénieur industriel de Pierrard-Virton / Sciences industrielles (MSIIA)", 'src_id' => 51 ],
            ['identity' => 'section_iv_msiie', 'label' => "Département ingénieur industriel de Pierrard-Virton / Sciences industrielles (MSIIE)", 'src_id' => 52 ],
            ['identity' => 'section_iv_msiip', 'label' => "Département ingénieur industriel de Pierrard-Virton / Sciences industrielles (MSIIP)", 'src_id' => 53],
            ['identity' => 'section_iv_tsi', 'label' => "Département ingénieur industriel de Pierrard-Virton / Sciences industrielles (TSI)", 'src_id' => 54 ],
            ['identity' => 'section_pa_birsg', 'label' => "Département paramédical Sainte Elisabeth / Bachelier infirmier responsable de soins généraux (BIRSG)", 'src_id' => 66 ],
            ['identity' => 'section_pa_sf', 'label' => "Département paramédical Sainte Elisabeth / Sage-femme (SF)", 'src_id' => 13 ],
            ['identity' => 'section_pa_bsi', 'label' => "Département paramédical Sainte Elisabeth / Soins infirmiers (BSI)", 'src_id' => 11 ],
            ['identity' => 'section_pa_soper', 'label' => "Département paramédical Sainte Elisabeth / Spécialisation en salle d'opération (SOPER)", 'src_id' => 15 ],
            ['identity' => 'section_pa_sc', 'label' => "Département paramédical Sainte Elisabeth / Spécialisation en santé communautaire (SC)", 'src_id' => 12 ],
            ['identity' => 'section_pa_siamu', 'label' => "Département paramédical Sainte Elisabeth / Spécialisation en soins intensifs et aide médicale urgente (SIAMU)", 'src_id' => 14 ],
            ['identity' => 'section_pb_npres', 'label' => "Département pédagogique de Bastogne / Normale préscolaire (NPRES)", 'src_id' => 35 ],
            ['identity' => 'section_pb_nprim', 'label' => "Département pédagogique de Bastogne / Normale primaire (NPRIM)", 'src_id' => 36 ],
            ['identity' => 'section_pb_nsbcp', 'label' => "Département pédagogique de Bastogne / Normale secondaire (NSBCP)", 'src_id' => 37 ],
            ['identity' => 'section_pb_nsff', 'label' => "Département pédagogique de Bastogne / Normale secondaire (NSFF)", 'src_id' => 38 ],
            ['identity' => 'section_pb_nsfr', 'label' => "Département pédagogique de Bastogne / Normale secondaire (NSFR)", 'src_id' => 39 ],
            ['identity' => 'section_pb_nsghs', 'label' => "Département pédagogique de Bastogne / Normale secondaire (NSGHS)", 'src_id' => 40 ],
            ['identity' => 'section_pb_nslg1', 'label' => "Département pédagogique de Bastogne / Normale secondaire (NSLG1)", 'src_id' => 41 ],
            ['identity' => 'section_pb_nslg2', 'label' => "Département pédagogique de Bastogne / Normale secondaire (NSLG2)", 'src_id' => 42 ],
            ['identity' => 'section_pb_nslg3', 'label' => "Département pédagogique de Bastogne / Normale secondaire (NSLG3)", 'src_id' => 43 ],
            ['identity' => 'section_pb_nsmat', 'label' => "Département pédagogique de Bastogne / Normale secondaire (NSMAT)", 'src_id' => 44 ],
            ['identity' => 'section_ch_npres', 'label' => "Département pédagogique de Champion / Normale préscolaire (NPRES)", 'src_id' => 22 ],
            ['identity' => 'section_ch_nprim', 'label' => "Département pédagogique de Champion / Normale primaire (NPRIM)", 'src_id' =>  23],
            ['identity' => 'section_ch_nsbcp', 'label' => "Département pédagogique de Champion / Normale secondaire (NSBCP)", 'src_id' => 24 ],
            ['identity' => 'section_ch_nslg1', 'label' => "Département pédagogique de Champion / Normale secondaire (NSLG1)", 'src_id' => 25 ],
            ['identity' => 'section_ch_nsmat', 'label' => "Département pédagogique de Champion / Normale secondaire (NSMAT)", 'src_id' => 28 ],
            ['identity' => 'section_ch_ortho', 'label' => "Département pédagogique de Champion / Spécialisation en orthopédagogie (ORTHO)", 'src_id' => 29 ],
            ['identity' => 'section_so_as', 'label' => "Département social de Namur / Assistant social (AS)", 'src_id' => 30 ],
            ['identity' => 'section_so_grh', 'label' => "Département social de Namur / Gestion des ressources humaines (GRH)", 'src_id' => 31 ],
            ['identity' => 'section_so_gsoc', 'label' => "Département social de Namur / Spécialisation en gestion du social (GSOC)", 'src_id' => 32 ],
            ['identity' => 'section_mi_asi', 'label' => "Département technique de Marche-en-Famenne / Architecture des systèmes informatiques (ASI)", 'src_id' => 57 ],
            ['identity' => 'section_es_em', 'label' => "Département électromécanique de Seraing / Électromécanique (EM)", 'src_id' => 50 ],
            ['identity' => 'section_es_mr', 'label' => "Département électromécanique de Seraing / Mécatronique et robotique (MR)", 'src_id' => 67 ],
            ['identity' => 'section_ma_bbd', 'label' => "Départements pédagogique et social de Malonne / Bibliothécaire - documentaliste (BBD)", 'src_id' => 16 ],
            ['identity' => 'section_ma_nprim', 'label' => "Départements pédagogique et social de Malonne / Normale primaire (NPRIM)", 'src_id' => 17 ],
            ['identity' => 'section_ma_nseph', 'label' => "Départements pédagogique et social de Malonne / Normale secondaire (NSEPH)", 'src_id' => 18 ],
            ['identity' => 'section_ma_nsff', 'label' => "Départements pédagogique et social de Malonne / Normale secondaire (NSFF)", 'src_id' => 19 ],
            ['identity' => 'section_ma_nsfr', 'label' => "Départements pédagogique et social de Malonne / Normale secondaire (NSFR)", 'src_id' => 20 ],
            ['identity' => 'section_ma_nsghs', 'label' => "Départements pédagogique et social de Malonne / Normale secondaire (NSGHS)", 'src_id' => 21 ],
            ['identity' => 'section_ma_grdm', 'label' => "Départements pédagogique et social de Malonne / Spécialisation en Gestion des ressources documentaires multimédia (GRDM)", 'src_id' => 61 ],
            ['identity' => 'section_iesn_cf', 'label' => "Départements économique, technique et pédagogique IESN / Comptabilité (CF)", 'src_id' => 2 ],
            ['identity' => 'section_iesn_cg', 'label' => "Départements économique, technique et pédagogique IESN / Comptabilité (CG)", 'src_id' => 3 ],
            ['identity' => 'section_iesn_cp', 'label' => "Départements économique, technique et pédagogique IESN / Comptabilité (CP)", 'src_id' => 4 ],
            ['identity' => 'section_iesn_dr', 'label' => "Départements économique, technique et pédagogique IESN / Droit (DR)", 'src_id' => 5 ],
            ['identity' => 'section_iesn_ig', 'label' => "Départements économique, technique et pédagogique IESN / Informatique de gestion (IG)", 'src_id' => 6 ],
            ['identity' => 'section_iesn_au', 'label' => "Départements économique, technique et pédagogique IESN / Informatique et systèmes (AU)", 'src_id' => 1 ],
            ['identity' => 'section_iesn_ir', 'label' => "Départements économique, technique et pédagogique IESN / Informatique et systèmes (IR)", 'src_id' => 68 ],
            ['identity' => 'section_iesn_ti', 'label' => "Départements économique, technique et pédagogique IESN / Informatique et systèmes (TI)", 'src_id' => 10 ],
            ['identity' => 'section_iesn_mk', 'label' => "Départements économique, technique et pédagogique IESN / Marketing (MK)", 'src_id' => 7 ],
            ['identity' => 'section_iesn_nseco', 'label' => "Départements économique, technique et pédagogique IESN / Normale secondaire (NSECO)", 'src_id' => 8 ],
            ['identity' => 'section_iesn_nsefs', 'label' => "Départements économique, technique et pédagogique IESN / Normale technique moyenne (NSEFS)", 'src_id' => 9 ],
            ['identity' => 'section_ea_ad', 'label' => "Départements économique, technique et social d'Arlon / Assistant de direction (AD)", 'src_id' => 60 ],
            ['identity' => 'section_ea_as', 'label' => "Départements économique, technique et social d'Arlon / Assistant social (AS)", 'src_id' => 58 ],
            ['identity' => 'section_ea_cf', 'label' => "Départements économique, technique et social d'Arlon / Comptabilité (CF)", 'src_id' => 45 ],
            ['identity' => 'section_ea_cg', 'label' => "Départements économique, technique et social d'Arlon / Comptabilité (CG)", 'src_id' => 46 ],
            ['identity' => 'section_ea_cp', 'label' => "Départements économique, technique et social d'Arlon / Comptabilité (CP)", 'src_id' => 47 ],
            ['identity' => 'section_ea_em', 'label' => "Départements économique, technique et social d'Arlon / Électromécanique (EM)", 'src_id' => 49 ],
        ];
        foreach( $sections as $key => $section)
            $sections[$key]['category_identity'] = 'section';

        // service communication
        $scs = [
            ['identity' => 'all', 'label'=> 'Tous', 'src_id' => 1 ],
            ['identity' => 'etu', 'label' => 'Etudiants', 'src_id' => 2 ],
            ['identity' => 'mdp', 'label' => 'Membres du personnel', 'src_id' => 3],
        ];
        foreach( $scs as $key => $sc )
            $scs[$key]['category_identity'] = 'general';

        // others
        $others = [
            [
                'identity' => 'henallux',
                'label'=> 'Widgets Henallux',
                'description' => "Liste des widgets fournit par l'institution",
                'category_identity' => 'bookmark',
                'src_id' => 1
            ],
        ];

        // datas
        $datas = [];
        $datas = array_merge($datas, $categories);
        $datas = array_merge($datas, $implantations);
        $datas = array_merge($datas, $departments);
        $datas = array_merge($datas, $sections);
        $datas = array_merge($datas, $scs);
        $datas = array_merge($datas, $others);

        // create
        foreach( $datas as $data )
        {
            $identity = $data['identity'];
            $label = $data['label'];
            $description = array_key_exists('description', $data) ? $data['description'] : null;

            $category = Category::where('identity', $data['category_identity'])->first();

            $group = new Group([
                'identity' => $identity,
                'label' => $label,
                'category_id' => $category->id,
                'description' => $description
            ]);
            $group->save();
        }

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
        //    Group::factory(20)->create();
        }
    }
}
