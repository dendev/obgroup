<?php

namespace Dendev\Obgroup\Database\Seeders;

use Dendev\Obgroup\Models\GroupMember;
use Dendev\Obgroup\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class GroupMemberSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     */
    public function run()
    {
        $this->_reset_table('group_members');

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            GroupMember::factory(25)->create();
        }
    }
}
