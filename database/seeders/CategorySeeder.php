<?php
namespace Dendev\Obgroup\Database\Seeders;

use Dendev\Obgroup\Models\Category;
use Dendev\Obgroup\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('categories' );

        $datas = [
            ['identity' => 'category', 'label' => 'Catégories', 'description' => ''],
            ['identity' => 'implantation', 'label' => 'Implantations', 'description' => ''],
            ['identity' => 'department', 'label' => 'Départements', 'description' => ''],
            ['identity' => 'section', 'label' => 'Sections', 'description' => ''],
            ['identity' => 'personal', 'label' => 'Personels', 'description' => ''],
            ['identity' => 'general', 'label' => 'Généraux', 'description' => ''],
            ['identity' => 'bookmark', 'label' => 'Liens', 'description' => ''],
        ];

        foreach( $datas as $data )
        {
            $category = new Category($data);
            $category->save();
        }

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            //Category::factory(15)->create();
        }
    }
}
