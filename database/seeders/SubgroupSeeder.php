<?php
namespace Dendev\Obgroup\Database\Seeders;

use Dendev\Obgroup\Models\Subgroup;
use Dendev\Obgroup\Traits\UtilSeeder;
use Illuminate\Database\Seeder;

class SubgroupSeeder extends Seeder
{
    use UtilSeeder;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->_reset_table('subgroups'); // warning adminSeeder !

        $datas = [
            ['label' => 'Etudiants', 'identity' => 'student', 'description' => null],
            ['label' => 'Mdp', 'identity' => 'mdp', 'description' => null],
            ['label' => 'Externes', 'identity' => 'extern', 'description' => null],
        ];

        foreach( $datas as $data )
        {
            $subgroup = new Subgroup($data);
            $subgroup->save();
        }

        if( env('FAKE_SEEDER_IS_ENABLED'))
        {
            //Subgroup::factory(15)->create();
        }
    }
}
