<?php

namespace Dendev\Obgroup\Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(CategorySeeder::class);

        $this->call(GroupSeeder::class);
        $this->call(SubgroupSeeder::class);

        $this->call(GroupMemberSeeder::class);
    }
}
