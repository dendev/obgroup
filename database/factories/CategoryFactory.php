<?php

namespace Dendev\Obgroup\Database\Factories;

use Dendev\Obgroup\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AnnouncementCategory>
 */
class CategoryFactory extends Factory
{

    protected $model = Category::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $label = $this->faker->sentence;

        return [
            'label' => $label,
            'identity' => Str::snake($label),
            'parent_id' => null,
            'description' => $this->faker->text,
            'order' => $this->faker->randomLetter,
        ];
    }
}
