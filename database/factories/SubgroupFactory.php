<?php

namespace Dendev\Obgroup\Database\Factories;

use Dendev\Obgroup\Models\Subgroup;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AnnouncementCategory>
 */
class SubgroupFactory extends Factory
{

    protected $model = Subgroup::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $label = $this->faker->sentence;

        return [
            'label' => $label,
            'identity' => Str::snake($label),
            'description' => $this->faker->text,
            'order' => $this->faker->randomLetter,
        ];
    }
}
