<?php

namespace Dendev\Obgroup\Database\Factories;

use Dendev\Obgroup\Models\Group;
use Dendev\Obgroup\Models\GroupMember;
use Dendev\Obgroup\Models\Subgroup;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Groups\GroupMember>
 */
class GroupMemberFactory extends Factory
{
    protected $model = GroupMember::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $group = Group::all()->random();

        $member_refs = [
            'users' => User::class,
            //'announcements' => Announcement::class, // FIXME util in package group ??
            //'bookmarks' => Bookmarks::class,
        ];

        $member_key = $this->faker->randomElement(array_keys($member_refs));
        $member_class = $member_refs[$member_key];

        $member = $member_class::all()->random();

        $is_user = $member_key === 'users';

        $already_exist = GroupMember::where('member_id', $member->id)
            ->where('member_table', $member_key)
            ->where('group_id', $group->id)
            ->get();

        if( $already_exist )
        {
            $group = Group::factory()->create();
        }

        $subgroup = Subgroup::all()->random(1)->first();

        return [
            'member_id' => $member->id,
            'member_table' => $member_key,
            'group_id' => $group->id,
            'subgroup_id' => $subgroup->id,
            'is_user' => $is_user,
            'user_can_view_members' => $this->faker->boolean,
            'user_can_remove_member' => $this->faker->boolean,
            'user_can_add_member' => $this->faker->boolean,
            //
        ];
    }
}
