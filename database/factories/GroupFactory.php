<?php

namespace Dendev\Obgroup\Database\Factories;

use Dendev\Obgroup\Models\Group;
use Dendev\Obgroup\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Groups\Group>
 */
class GroupFactory extends Factory
{
    protected $model = Group::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $label = $this->faker->sentence(6, true);
        $category = Category::all()->random();

        return [
            'label' => $label,
            'identity' => \Str::slug($label),
            'category_id' => $category->id,
            'description' => $this->faker->text(),
            'order' => $this->faker->randomLetter(),
        ];
    }
}
