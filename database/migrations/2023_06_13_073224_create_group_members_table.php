<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('group_members', function (Blueprint $table) {
            $table->id();
            $table->string('member_table');
            $table->foreignId('member_id');
            $table->foreignId('group_id')->constrained();
            $table->foreignId('subgroup_id')->constrained();
            $table->boolean('is_user')->default(false);
            $table->boolean('user_can_view_members')->default(false);
            $table->boolean('user_can_remove_member')->default(false);
            $table->boolean('user_can_add_member')->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->unique(['member_id', 'member_table', 'group_id']);
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('group_members');
    }
};
